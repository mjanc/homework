const enviroment = 'production';
const domain = ((enviroment == 'dev') ? 'http://localhost/websites/bens-consulting' : 'http://tvojnet.si');
const app = angular.module('bens', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('login', {
        url: '/',
        templateUrl: 'partials/login.html'
    })
    .state('form', {
        url: '/form',
        templateUrl: 'partials/form.html',
        resolve: {
            loggedIn: function(){
                if(localStorage.getItem("loggedIn") === null)
                    window.location = domain;
            }
        }
    })
    .state('history', {
        url: '/history',
        templateUrl: 'partials/history.html',
        resolve: {
            loggedIn: function(){
                if(localStorage.getItem("loggedIn") === null)
                    window.location = domain;
            }
        }
    });
});

/**
 * Header Controller
 */
app.controller('headerController', function ($scope, $location) {
    $scope.loginText = 'Login';
    if(localStorage.getItem("loggedIn"))
        $scope.loginText = 'Logout';

    $scope.loginStatus = function () {
        if(localStorage.getItem("loggedIn"))
            localStorage.removeItem("loggedIn");

        $location.path("/");
    };
});

/**
 * Login Controller
 */
app.controller('loginController', function($scope, $http, $location) {
    $scope.formData = {};

    $scope.submit = function() {
        $http.post(domain+'/server/login.php', this.formData)
            .then(function (res) {
                $scope.msg = '';
                const data = res.data;
                if(data.hasOwnProperty('id')) {
                    localStorage.setItem("loggedIn", data.id);
                    $location.path('form');
                }
                else {
                    $scope.msg = data.msg;
                }
            });
    };
});

/**
 * Content Controller
 */
app.controller('contentController', function ($scope, $http, $location) {
    $scope.formData = {};

    $scope.submit = function() {
        $http.post(domain+'/server/history.php', this.formData)
            .then(function (res) {
                $location.path("/history");
            });
    };

    $scope.clear = function () {
        document.getElementById("inputForm").reset();
    };
});

/**
 * History Controller
 */
app.controller('historyController', function ($scope, $http, $location) {

    $http.get(domain+'/server/history.php')
        .then(function (res) {
            const results = [];
            for(let item of res.data.results) {
                let json = JSON.parse(item[1]);
                json['date'] = item[2];
                results.push(json);
            }
            $scope.records = results;
        });
});